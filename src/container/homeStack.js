import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import HomeScreen from './HomeScreen';
import Quiz from './Quiz';
import previousScores from './previousScores';
import scoreScreen from './scoreScreen';
import responseScreen from './responseScreen';
import {HeaderBackButton} from 'react-navigation';

const screens = {
  HomeScreen: {
    screen: HomeScreen,
  },
  Quiz: {
    screen: Quiz,
  },
  previousScores: {
    screen: previousScores,
  },
  scoreScreen: {
    screen: scoreScreen,
  },
  responseScreen: {
    screen: responseScreen,
  },
};

const HomeStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    headerShown: false,
  },
});

export default createAppContainer(HomeStack);
