import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Modal,
  TextInput,
  Alert,
} from 'react-native';
import React from 'react';
import image1 from '../assets/quizLogo.png';
import {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function HomeScreen({navigation}) {
  const [modalVisible, setModalVisible] = useState(false);
  const [userinput, setInput] = useState('');
  const [exist, setExist] = useState([]);
  const [sta, setSta] = useState([]);
  const [flag, setFlag] = useState(true);
  const pressHandler = () => {
    setSta(prev1 => [...prev1, "hii"]);
    setModalVisible(true);
    // navigation.navigate('Quiz')
  };
  const enterFunc = () => {
    if (!exist.includes(userinput)) {
      if (userinput.length >= 3) {
        setModalVisible(false);
        navigation.navigate('Quiz', {
          paramKey: userinput,
        });
      } else {
        Alert.alert('The name should of minimum of 3letters');
      }
    } else {
      Alert.alert('the username already exists');
    }
  };
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key');
      // console.log({value});
      if (value != null) {
        // console.log(value)
        a = JSON.parse(value);
        for (i in a) {
          setExist(prev1 => [...prev1, a[i]['user']]);
        }
        // console.log({a});
      }

      // setArr(navigation.getParam('paramKey'))
    } catch (e) {
      // error reading value
    }
  };

  const textInpu = text => {
    text = text.trim();
    setInput(text);
    // console.log(text)
  };
  useEffect(() => {
    console.log(exist);
  }, [exist]);

  useEffect(() => {
    console.log('useEffect is called');
    getData();
  }, [sta]);

  return (
    <View style={styles.main}>
      <View style={styles.quiz}>
        <Text style={styles.text}>Quiz</Text>
      </View>
      <View style={styles.viewLogo}>
        <Image style={styles.logo} source={image1} />
      </View>
      <View style={styles.modal}>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
              setModalVisible(!modalVisible);
            }}>
            <View style={styles.modalText}>
              <Text style={styles.text1}>Enter your name</Text>
              <TextInput
                style={styles.TextInput}
                onChangeText={text => textInpu(text)}></TextInput>
              <View style={styles.bottom}>
                <TouchableOpacity>
                  <Text
                    style={styles.cancel}
                    onPress={() => setModalVisible(!modalVisible)}>
                    {' '}
                    Cancel
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => enterFunc()}>
                  <Text style={styles.enter}> Enter</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
        <View>
          <TouchableOpacity style={styles.takeTest} onPress={pressHandler}>
            <Text style={styles.textfile}>Take test</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('previousScores')}>
            <View style={styles.previousScores}>
              <Text>Previous Scores</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  text: {fontSize: 30},
  logo: {width: 150, height: 150, borderRadius: 40},
  main: {
    // flex:1,
    backgroundColor: '#6DD5FA',
    justifyContent: 'space-around',
    flex: 1,
    alignItems: 'center',
  },
  takeTest: {
    // flex:1,
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    width: 300,
    alignItems: 'center',
    // top: 500,
  },
  modalText: {
    // flex:1,
    alignItems: 'center',
    marginTop: 270,
    padding: 20,
    width: 300,
    height: 150,
    backgroundColor: 'white',
    marginHorizontal: 50,
  },
  TextInput: {
    // flex:1,
    borderBottomWidth: 1,
    marginBottom: 10,
    width: 250,
  },
  text1: {fontSize: 20},
  bottom: {
    // flex:1
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignContent: 'flex-end',
    // justifyContent:'space-between'
  },
  previousScores: {
    marginLeft: 100,
  },
  cancel: {
    padding: 10,
    // backgroundColor:'red',
    marginRight: 150,
    fontSize: 20,
    color: 'skyblue',
  },
  enter: {
    padding: 10,
    fontSize: 20,
    color: 'skyblue',
  },
});
