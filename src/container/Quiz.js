import {
  View,
  Text,
  Button,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import RadioButtonRN from 'radio-buttons-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Radiobutton from './Radiobutton';
var ques_no = 1;
var question_idd = 0;
var question_no = 0;
var score = 0;
var empArr = [];
var answers = {};
``;
export default function Quiz({navigation}) {
  const [question_idd, setQues_id] = useState(0);
  const [question_no, setQues_no] = useState(0);
  const [ques_no, setQues] = useState(1);
  var user = navigation.getParam('paramKey');
  console.log(navigation.getParam('paramKey'));
  const [selc, setSelc] = useState('');
  const [nextbutton, setNextB] = useState('Next');
  const [flag, setFlag] = useState(true);
  const [curr, setCurr] = useState(-1);
  console.log('hhh', selc[question_no]);
  console.log(flag);

  const onPressRadioButton = obj => {
    setSelc(obj);
    setCurr(question_no);
    setFlag(false);
    console.log('hii');

    //  console.log(selc)

    // setSelc(index)
    // r

    // console.log(selectedValue,index)
    // if(selectedValue==data.questionsData[question_idd])
  };
  const jo = {1: 'hii'};
  const [nex, setNex] = useState(1);
  const data1 = [
    {
      la: 'West',
    },
    {
      la: 'data 2',
    },
  ];
  const data = {
    questionsData: [
      {
        question_id: 'fdsfss',
        question: 'Sun rises at?',
        options: [
          {
            option_id: 'sdfs',
            option: 'West',
          },
          {
            option_id: 'fdssfs',
            option: 'East',
          },
          {
            option_id: 'fdssfsfds',
            option: 'North',
          },
          {
            option_id: 'fdsfssfs',
            option: 'South',
          },
        ],
        correct_option: 'fdssfs',
      },
      {
        question_id: 'fdsgdfdtgdgfss',
        question: 'Capital of India?',
        options: [
          {
            option_id: 'sdadssdfs',
            option: 'Hyderabad',
          },
          {
            option_id: 'uagd67ds',
            option: 'Mumbai',
          },
          {
            option_id: '76atdgutdu',
            option: 'New Delhi',
          },
          {
            option_id: 'iugduys9',
            option: 'Bangalore',
          },
        ],
        correct_option: '76atdgutdu',
      },
      {
        question_id: 'bckkguszds',
        question: 'National game of India?',
        options: [
          {
            option_id: 'bvhcxv',
            option: 'Cricket',
          },
          {
            option_id: 'jsbhdsfk',
            option: 'Foot ball',
          },
          {
            option_id: 'iusyfihbjfs',
            option: 'Chess',
          },
          {
            option_id: 'uf8isb8is',
            option: 'Hockey',
          },
        ],
        correct_option: 'uf8isb8is',
      },
      {
        question_id: 'mshcugsjsyfd',
        question: 'National animal of India?',
        options: [
          {
            option_id: 'mdisgdyfsujgdauv',
            option: 'Tiger',
          },
          {
            option_id: 'vuhfh',
            option: 'Lion',
          },
          {
            option_id: 'vcysacdhf',
            option: 'Cheetah',
          },
          {
            option_id: 'kdhaihksdbf',
            option: 'Rhino',
          },
        ],
        correct_option: 'mdisgdyfsujgdauv',
      },
      {
        question_id: 'fdstrgdhfsdfss',
        question: 'How many states in India?',
        options: [
          {
            option_id: '9oikbdjsgduygs',
            option: '21',
          },
          {
            option_id: 'hjfsdytrs6',
            option: '22',
          },
          {
            option_id: 'hjusgfdyut7',
            option: '28',
          },
          {
            option_id: '76yrafdygvsg',
            option: '29',
          },
        ],
        correct_option: '76yrafdygvsg',
      },
      {
        question_id: 'xghfytdytxfg',
        question: 'Capital of Telangana?',
        options: [
          {
            option_id: 'xtdgb',
            option: 'Allahbad',
          },
          {
            option_id: 'tdtrcfg',
            option: 'Hyderabad',
          },
          {
            option_id: 'ohcgdd',
            option: 'Bangalore',
          },
          {
            option_id: 'tretvghd',
            option: 'Adilabad',
          },
        ],
        correct_option: 'tdtrcfg',
      },
    ],
  };
  for (let i = 0; i <= data.questionsData.length - 1; i++) {
    answers[i] = data.questionsData[i].correct_option;
    for (let j = 0; j <= data.questionsData[i].options.length - 1; j++) {
      data.questionsData[i].options[j]['label'] =
        data.questionsData[i].options[j]['option'];
      delete data.questionsData[i].options[j]['option'];
    }
  }
  const [next, setNext] = useState(data.questionsData[0].options);
  const [questions, setQuest] = useState(data.questionsData[0].question);
  const [obj, setObj] = useState({0: ''});
  const [fla, setFla] = useState(false);

  var array = data.questionsData[question_idd].options.map(res => ({
    option_id: res.option_id,
    option: res.label,
  }));
  const func = () => {
    if (nextbutton == 'Submit') {
      navigation.pop();
      navigation.navigate('scoreScreen', {
        paramKey: selc,
        answer: answers,
        username: user,
        data: data,
      });
    } else if (question_no != data.questionsData.length - 1) {
      setQues_id(question_idd + 1);
      setQues_no(question_no + 1);
      setQues(ques_no + 1);
      setNext(data.questionsData[question_idd + 1].options);
      setQuest(data.questionsData[question_no + 1].question);
      // if (selc[question_no - 1]) {
      //       console.log('jjjjjjjj');
      //       setFlag(false);
      //     } else {
      //       setFlag(true);
      //     }

      // console.log(nex)
      // r
    } else {
      setNextB('Submit');
    }
    setFlag(true);
  };
  const prev = () => {
    if (question_no != 0) {
      setQues_id(question_idd - 1);
      setQues_no(question_no - 1);
      setQues(ques_no - 1);
      setNext(data.questionsData[question_idd - 1].options);
      setQuest(data.questionsData[question_no - 1].question);
      ques_no - 1;
      if (selc[question_no - 1]) {
        console.log('jjjjjjjj');
        setFlag(false);
      }
    }
  };
  useEffect(() => {
    if (selc[question_no]) {
      console.log('jjjjjjjj');
      setFlag(false);
    }
  });
  console.log(selc, question_no);
  //  console.log(flag)

  const naviQues = index => {
    if (selc[index] || selc[index - 1]) {
      console.log('hjjjoo', index);
      setQues_id(index);
      setQues_no(index);
      setQues(index + 1);
      setNext(data.questionsData[index].options);
      setQuest(data.questionsData[index].question);
    }
  };
  return (
    <View style={styles.main}>
      <View style={styles.quesNav}>
        <View>
          <FlatList
            horizontal
            data={data['questionsData']}
            renderItem={({index}) => {
              return (
                <View>
                  <TouchableOpacity onPress={() => naviQues(index)}>
                    <View
                      style={{
                        ...styles.quesNav1,
                        ...{
                          backgroundColor:
                            question_no == index
                              ? 'purple'
                              : selc[index]
                              ? 'lightgreen'
                              : 'white',
                        },
                        ...{
                          opacity: selc[index]
                            ? 1
                            : curr + 1 == index
                            ? 1
                            : 0.2,
                        },
                        // ...{
                        //    backgroundColor: curr[index]==index ? 'purple':''
                        // }
                      }}>
                      <View style={styles.numbers}>
                        <Text>{index + 1} </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      </View>
      <View style={styles.questions}>
        <Text style={styles.question11}>
          {ques_no + ')' + JSON.stringify(questions)}
        </Text>
      </View>
      {/* <Text>{JSON.stringify(array)}</Text> */}

      {/* <RadioButtonRN data={next}
      icon={
        <Icon
          name="rocket"
          size={15}
          color="#2c9dd1"
          selectedBtn={(e) => console.log(e)}
        />
      }
      
      /> */}
      <View style={styles.options}>
        <Radiobutton
          ques={question_idd}
          onPress={onPressRadioButton}
          data={array}
        />
      </View>

      <View style={styles.button}>
        {console.log(flag)}
        <View style={styles.button_Prev}>
          <TouchableOpacity style={styles.prev} onPress={prev}>
            <Text>Prev</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.next} disabled={flag} onPress={func}>
            <Text>{nextbutton}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {flex: 1},
  next: {
    // alignItems: 'center',
    //  marginLeft:318,
    backgroundColor: 'skyblue',
    padding: 20,
    width: 120,
    alignItems: 'center',
    // top: 500,
  },
  questions: {
    flex: 0.5,
    justifyContent: 'center',
  },
  options: {
    flex: 2,
    // backgroundColor:'blue'
  },
  prev: {
    backgroundColor: 'skyblue',
    padding: 20,
    width: 120,
    alignItems: 'center',

    //  bottom:40
  },
  button_Prev: {
    alignContent: 'center',
  },
  question11: {
    fontSize: 30,
    color: 'black',
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems:'center'
  },
  quesNav: {
    flex: 0.3,
    // backgroundColor: 'skyblue',
    alignContent: 'center',
    justifyContent: 'center',
  },
  quesNav1: {
    padding: 10,
    // backgroundColor: 'blue',
    marginHorizontal: 20,
    borderRadius: 100,
    justifyContent: 'center',
  },
  numbers: {
    justifyContent: 'center',
  },
});
