import {View, Text, FlatList} from 'react-native';
import React from 'react';

export default function responseScreen({navigation}) {
  console.log(navigation.getParam('paramKey'));

  return (
    <View>
      <Text>responseScreennavig</Text>
      <FlatList
        data={navigation.getParam('paramKey')}
        renderItem={({item}) => {
          return (
            <View>
              <View style={{marginBottom: 20}}>
                <View
                  style={{
                    backgroundColor:
                      item.selected === item.correct ? 'lightgreen' : '#ffcccb',
                  }}>
                  <Text>{item.question}</Text>
                  <Text>{item.option1}</Text>
                  <Text>{item.option2}</Text>
                  <Text>{item.option3}</Text>
                  <Text>{item.option4}</Text>

                  <View>
                    <Text>
                      {item.selected === item.correct ? '\u2705' : '\u274C'}
                    </Text>
                  </View>

                  <View style={{}}>
                    <Text style={{textAlign: 'right'}}>
                      Selected Answer: {item.selected} | Correct Answer:{' '}
                      {item.correct}
                    </Text>
                  </View>
                </View>
              </View>
              <View></View>
            </View>
          );
        }}
      />
    </View>
  );
}
