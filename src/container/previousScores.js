import {View, Text, FlatList,StyleSheet,TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function previousScores({navigation}) {
  const [scor, setScore] = useState([]);
  const [respos,setRespos]=useState([])
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key');
      const value1 = await AsyncStorage.getItem('@storage_Key1');
      // console.log({value});
      if (value != null) {
        // console.log(value)
        a = JSON.parse(value);
        b= JSON.parse(value1)
        // console.log({a});
        setScore(a);
        setRespos(b)
      }
      // setArr(navigation.getParam('paramKey'))
    } catch (e) {
      // error reading value
    }
  };

  useEffect(() => {
    // console.log(scor);
    // console.log(respos)
  }, [scor,respos]);

  useEffect(() => {
    getData();
  }, []);
  console.log(respos)
  return (
    <View style={styles.main}>
      <View style={styles.previousScores}>
        <View>
          <Text style={{fontSize: 20}}>username</Text>
        </View>
        <View>
          <Text style={{fontSize: 20}}>score</Text>
        </View>
      </View>
      {/* <Text>{JSON.parse(navigation.getParam('paramKey'))}</Text> */}
      <View style={styles.FlatList}>
        <FlatList
          data={scor}
          renderItem={({item, index}) => (
            <View>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('responseScreen', {
                    paramKey: respos[index]
                  
                  })
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 20,
                    marginHorizontal: 80,
                  }}>
                  <View>
                    <Text style={{fontSize: 20}}>{item.user}</Text>
                  </View>
                  <View>
                    <Text style={{fontSize: 20}}>{item.sco}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  main:{
    flex:1
  },
  previousScores:{
    flex:0.15,
    alignItems:'center',
    flexDirection:'row',
    marginHorizontal:60,
    justifyContent:'space-between'

  },
  FlatList:{
    flex:1
  }

})