import { View, Text ,StyleSheet,TouchableOpacity} from 'react-native'
import React, { useState } from 'react'
import { Colors } from 'react-native/Libraries/NewAppScreen'

export default function Radiobutton({ques, onPress,data,obj,flag}) {
    const [colorChange,setChangecolor]=useState('white')
    const [currIndex,setActiveIndex]=useState("")
    const [currOpt,setCurr]=useState({})
    array=data.map(res=>res.name)
    const func=(index) => {
        setActiveIndex(index)
        setCurr((prev)=>{
            prev[ques]=index
            return prev
        })
        
        
        // console.log(currIndex)
        // console.log(index,currIndex)
        
        if(index==currIndex){
            // console.log("hii")
            setChangecolor('black')
            
        // if(colorChange==='white'){
        //     console.log("hello")
        // setChangecolor('black')
        // }
        // else{
        // setChangecolor('white')
        // }
    }

    }
    const ren=()=>{
        return data.map((element)=>{
            return (
                <View>      
    <TouchableOpacity style={{...styles.touch,...{backgroundColor:(currOpt[ques]==element.option_id)? 'skyblue' :'white'}}} onPress={() =>{  onPress(currOpt),func(element.option_id)}}>
        <View style = {{flexDirection: 'row', alignItems: 'center'}}>
        <View>
    <View style={styles.radioOuter}>
        {/* {console.log(currOpt[ques],index)} */}
    <View style={{...styles.radioInner,...{backgroundColor:(currOpt[ques]==element.option_id)? 'black' :'white'}}}>
            
            </View>

        {/* {console.log(currIndex,index,"llll",flag)} */}
       
        
    </View>
    </View>
    <View style={{marginLeft:10}}>
    <Text style={styles.option}>{element.option}</Text>
    </View>
    </View>
    </TouchableOpacity>
    </View>
            )
        })
    }

  return (
    <View>      
    {ren()}
    </View>
        
  )
}

const styles = StyleSheet.create({
    radioOuter:{width:30,height:30,borderRadius:15,borderWidth:1,borderColor:'black',justifyContent:'center',alignItems:'center'},
    radioInner:{width:20,height:20,borderRadius:10,borderWidth:0.5,borderColor:'white'},
    touch:{borderWidth:1,marginBottom:20,borderRadius:20,padding:25},
    option:{color:'black'}
})