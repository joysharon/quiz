import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
var obj = {
  question: '',
  option1: '',
  option2: '',
  option3: '',
  option4: '',
  correct: '',
  selected: '',
  userName: ' ',
  score: ' ',
};
var dataObj = '';
const fun11 = () => {
  console.log(dataObj);
};

export default function scoreScreen({navigation}) {
  var score = 0;
  const list = [];
  //   console.log(navigation.getParam('paramKey'));
  //   console.log(navigation.getParam('answer'));
  var data = navigation.getParam('data');
  dataObj = navigation.getParam('data');

  //   console.log('hiii')
  const [arr, setArr] = useState([]);
  const [res, setRes] = useState([]);
  // console.log(obj)

  for (var i in navigation.getParam('paramKey')) {
    obj = {
      question: '',
      option1: '',
      option2: '',
      option3: '',
      option4: '',
      correct: '',
      selected: '',
      userName: ' ',
      score: ' ',
    };

    obj['question'] = data.questionsData[i].question;
    obj['option1'] = data.questionsData[i].options[0].label;
    obj['option2'] = data.questionsData[i].options[1].label;
    obj['option3'] = data.questionsData[i].options[2].label;
    obj['option4'] = data.questionsData[i].options[3].label;
    console.log(data.questionsData[i].options.length);
    for (var j = 0; j <= data.questionsData[i].options.length - 1; j++) {
      // console.log(i,j)
      // console.log(navigation.getParam('paramKey')[i],data.questionsData[i].options[j].option_id)
      if (
        data.questionsData[i].options[j].option_id ==
        navigation.getParam('paramKey')[i]
      ) {
        obj['selected'] = data.questionsData[i].options[j].label;
      }
      if (
        data.questionsData[i].options[j].option_id ==
        navigation.getParam('answer')[i]
      ) {
        obj['correct'] = data.questionsData[i].options[j].label;
      }
    }
    // if(navigation.getParam('paramKey')[i]==data.questionsData[i].options[0].option_id){

    // }
    // console.log(obj)

    list.push(obj);
    // console.log(navigation.getParam('paramKey')[i]==navigation.getParam('answer')[i])
    if (
      navigation.getParam('paramKey')[i] == navigation.getParam('answer')[i]
    ) {
      score += 1;
      //     console.log(score)
    }
  }
  //   console.log(list.length);
  obj['userName'] = navigation.getParam('username');
  obj['score'] = score;

  const storeData = async updatedScoreData => {
    try {
      const jsonValue = JSON.stringify(updatedScoreData);
      const jsonValue1 =JSON.stringify(res)
      console.log(arr);
      console.log("objjjj",res)
      //   console.log(jsonValue)
      AsyncStorage.setItem('@storage_Key', jsonValue);
      AsyncStorage.setItem('@storage_Key1', jsonValue1);
    } catch (e) {
      // saving error
      console.log(e);
    }
  };

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key');
      const value1 = await AsyncStorage.getItem('@storage_Key1');

      if (value != null) {
        // console.log(value)
        a = JSON.parse(value);
        setArr([...a, {user: navigation.getParam('username'), sco: score}]);
      } else {
        setArr(prev => [
          ...prev,
          {user: navigation.getParam('username'), sco: score},
        ]);
      }

        if (value1 != null) {
          b = JSON.parse(value1);
          setRes([...b, list]);
        } else {
          setRes(prev1 => [...prev1, list]);
        }

      //  storeData([...arr,{user:navigation.getParam('username'),sco:score}])
    } catch (e) {
      // error reading value
    }

  
  };

  useEffect(() => {
    if (arr.length > 0) {
      storeData(arr);
    }
  }, [arr,res]);

  useEffect(() => {
    getData();
  }, []);
  
  console.log(obj)

  return (
    <View style={styles.main}>
      <View style={styles.score}>
        <View>
          <Text>scoreScreen</Text>
          <Text>{navigation.getParam('username')}</Text>
          <Text>your score is {score}</Text>
        </View>
      </View>
      {/* <Text>{JSON.stringify(arr)}</Text> */}

      <View style={styles.response}>
        <FlatList
          data={list}
          renderItem={({item}) => {
            return (
              <View>
                <View style={{marginBottom: 20}}>
                  <View
                    style={{
                      backgroundColor:
                        item.selected === item.correct
                          ? 'lightgreen'
                          : '#ffcccb',
                    }}>
                    <Text>{item.question}</Text>
                    <Text>{item.option1}</Text>
                    <Text>{item.option2}</Text>
                    <Text>{item.option3}</Text>
                    <Text>{item.option4}</Text>

                    <View>
                      <Text>
                        {item.selected === item.correct ? '\u2705' : '\u274C'}
                      </Text>
                    </View>

                    <View style={{}}>
                      <Text style={{textAlign: 'right'}}>
                        Selected Answer: {item.selected} | Correct Answer:{' '}
                        {item.correct}
                      </Text>
                    </View>
                  </View>
                </View>
                <View></View>
              </View>
            );
          }}
        />
      </View>
      <View style={styles.previous}>
        <View>
          <TouchableOpacity
            style={styles.previousTouch}
            onPress={() => {
              navigation.navigate('previousScores', {
                paramKey: arr,
              });
            }}>
            <Text>previousScores</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            style={styles.previousTouch}
            onPress={() => {
              navigation.navigate('HomeScreen');
            }}>
            <Text>Take Test again</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    margintop: 10,
    flex: 1,
  },
  score: {
    flex: 0.15,
    justifyContent: 'center',
    // alignContent: 'center',
  },
  response: {
    flex: 1,
  },
  previous: {
    flex: 0.15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  previousTouch: {
    padding: 10,
    width: 100,
    marginHorizontal: 35,
    backgroundColor: 'skyblue',
  },
});

console.log(dataObj);
